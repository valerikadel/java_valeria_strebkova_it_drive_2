create table if not exists users
(
    id       serial                     not null
        constraint user_pk
            primary key,
    login    varchar(25)                not null,
    password bit varying(30)            not null,
    token    uuid,
    role     varchar(40) default 'USER' not null
);

alter table users
    owner to postgres;

create unique index if not exists users_id_uindex
    on users (id);

create table if not exists user_file
(
    id           serial       not null
        constraint user_file_pk
            primary key,
    user_id      integer
        constraint user_file_people_id_fk
            references users,
    file_name    varchar(125) not null,
    local_path   varchar(500),
    content_type varchar(100)
);

alter table user_file
    owner to postgres;

create unique index if not exists user_file_id_uindex
    on user_file (id);

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

create table rooms
(
    id         serial      not null
        constraint room_pk primary key,
    creator_id integer
        constraint room_users_id_fk
            references users,
    name       varchar(50) not null
);

create table tomcat_sessions (
    id varchar(100) not null primary key ,
    valid char(1) not null,
    maxinactive int not null,
    lastaccess bigint,
    data bytea,
app varchar(20)
);
create table room_users
(
    id      serial not null
        constraint room_users_id primary key,
    room_id int    not null
        constraint
            room_users_room_id_fk references rooms,
    user_id int    not null
        constraint room_users_user_id references users
);
