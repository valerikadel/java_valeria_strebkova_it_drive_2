package ru.itdrive.service;

import ru.itdrive.models.File;

import java.util.List;

public interface FileService {

    File save(File file, String token);

    void delete(Integer id);

    List<File> findAllByUser(String token);
}
