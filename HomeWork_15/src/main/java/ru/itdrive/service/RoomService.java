package ru.itdrive.service;

import ru.itdrive.models.Room;

import java.util.List;

public interface RoomService {
    Room save(Room room, String token);

    void delete(Integer id);

    List<Room> findByUser(String token);

    Room addUserInRoom(Integer userId, Integer roomId);


    Room removeUserFromRoom(Integer userId, Integer roomId);
}
