package ru.itdrive.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itdrive.models.File;
import ru.itdrive.models.User;
import ru.itdrive.repository.FileRepository;
import ru.itdrive.service.FileService;
import ru.itdrive.service.UserService;

import java.util.List;

@Service
@AllArgsConstructor
public class FileServiceImpl implements FileService {
    private final FileRepository repository;
    private final UserService userService;

    @Override
    public File save(File file, String token) {
        User user = userService.findByToken(token);
        file.setCreator(user);
        return repository.save(file);
    }

    @Override
    public void delete(Integer id) {
        repository.delete(id);
    }

    @Override
    public List<File> findAllByUser(String token) {
        return repository.getFiles(token);
    }
}
