package ru.itdrive.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itdrive.models.Authentication;
import ru.itdrive.models.User;
import ru.itdrive.service.SignService;
import ru.itdrive.service.UserService;

import java.util.Optional;

@Service
@AllArgsConstructor
public class SignServiceImpl implements SignService {
    private final UserService userService;

    @Override
    public Authentication authenticate(String login, String password) {
        User user = userService.findByEmail(login);
        if (Optional.ofNullable(user).isPresent()) {
            return Authentication.builder()
                    .email(user.getLogin())
                    .password(user.getPassword())
                    .role(user.getRole()).build();
        } else {
            return register(login, password);
        }
    }

    @Override
    public Authentication register(String login, String password) {
        User newUser = userService.save(User.builder()
                .login(login)
                .password(password)
                .build());
        return Authentication.builder()
                .password(newUser.getPassword())
                .email(newUser.getLogin())
                .role(newUser.getRole())
                .build();
    }
}
