package ru.itdrive.service.impl;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.stereotype.Service;
import ru.itdrive.models.Room;
import ru.itdrive.models.User;
import ru.itdrive.repository.RoomRepository;
import ru.itdrive.service.RoomService;
import ru.itdrive.service.UserService;

import java.util.List;

@Service
@AllArgsConstructor
public class RoomServiceImpl implements RoomService {
    private final RoomRepository repository;
    private final UserService userService;

    @Override
    public Room save(Room room, String token) {
        User currentUser = userService.findByToken(token);
        room.setCreator(currentUser);
        return repository.save(room);
    }

    @Override
    public void delete(@NonNull Integer id) {
        repository.delete(id);
    }

    @Override
    public List<Room> findByUser(String token) {
        return repository.findAll(token);
    }

    @Override
    public Room addUserInRoom(@NonNull Integer userId, @NonNull Integer roomId) {
        return repository.addUserInRoom(userId, roomId);
    }

    @Override
    public Room removeUserFromRoom(@NonNull Integer userId, @NonNull Integer roomId) {
        return repository.removeUserFromRoom(userId, roomId);
    }
}
