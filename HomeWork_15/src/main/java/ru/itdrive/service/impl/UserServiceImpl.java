package ru.itdrive.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itdrive.models.User;
import ru.itdrive.repository.UserRepository;
import ru.itdrive.service.UserService;

import java.util.List;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository repository;

    @Override
    public User save(User user) {
        return repository.save(user);
    }

    @Override
    public List<User> findAll() {
        return repository.findAll();
    }

    @Override
    public User findByEmail(String email) {
        return repository.findByLogin(email);
    }

    @Override
    public User findByToken(String token) {
        return repository.findByToken(token);
    }
}
