package ru.itdrive.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public interface DatabaseClosing {
    default void close(Connection connection, PreparedStatement statement, ResultSet resultSet) {
        Optional.ofNullable(resultSet).ifPresent(r -> {
            try {
                r.close();
            } catch (SQLException e) {
                throw new IllegalArgumentException(e);
            }
        });
        Optional.ofNullable(statement).ifPresent(st -> {
            try {
                st.close();
            } catch (SQLException e) {
                throw new IllegalArgumentException(e);
            }
        });
        Optional.ofNullable(connection).ifPresent(con -> {
            try {
                con.close();
            } catch (SQLException e) {
                throw new IllegalArgumentException(e);
            }
        });
    }
}
