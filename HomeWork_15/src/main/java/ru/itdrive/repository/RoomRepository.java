package ru.itdrive.repository;

import lombok.NonNull;
import ru.itdrive.models.Room;

import java.util.List;

public interface RoomRepository extends CrudRepository<Room> {
    List<Room> findAll(@NonNull String token);

    Room addUserInRoom(Integer userId, Integer roomId);

    Room removeUserFromRoom(Integer userId, Integer roomId);

    Room findById(Integer id);
}
