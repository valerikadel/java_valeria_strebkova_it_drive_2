package ru.itdrive.repository;

import ru.itdrive.models.User;

public interface UserRepository extends CrudRepository<User> {
    User findByLogin(String login);

    User findByToken(String token);
}
