package ru.itdrive.repository.impl;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.stereotype.Component;
import ru.itdrive.models.Room;
import ru.itdrive.models.User;
import ru.itdrive.repository.RoomRepository;
import ru.itdrive.repository.RowMapper;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor
public class RoomRepositoryImpl implements RoomRepository {
    //language=SQL
    private final String SQL_INSERT = "insert into rooms (creator_id, name) values (?, ?)";
    private final String SQL_UPDATE = "update rooms set name = ? where id = ?";
    private final String SQL_DELETE = "delete from rooms where id = ?";
    private final String SQL_SELECT_ALL_BY_USER = "select r.id as rId, r.name as rName " +
            "from rooms r " +
            "inner join users u on r.creator_id = u.id " +
            "where u.token = ?";
    //language=SQL
    private final String SQL_ADD_USER_IN_ROOM = "insert into room_users (room_id, user_id) values (?, ?)";
    private final String SQL_REMOVE_USER_FROM_ROOM = "delete from room_users where room_id = ? and user_id = ?";
    private final String SQL_SELECT_BY_ID = "select r.id as rId, r.name as rName, " +
            "u.id as uId, u.email as uLogin " +
            "from room r " +
            "inner join room_users ru on r.id = ru.room_id " +
            "inner join users u on u.id = ru.user_id " +
            "where r.id =? ";
    private final RowMapper<Room> roomRowMapper = row -> Room.builder()
            .id(row.getInt("rId"))
            .name(row.getString("rName"))
            .build();
    private final RowMapper<User> userRowMapper = row -> User.builder()
            .id(row.getInt("uId"))
            .login(row.getString("uLogin"))
            .build();
    private DataSource dataSource;

    @Override
    public Room save(@NonNull Room object) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            if (object.getId() == null) {
                statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
                statement.setInt(1, object.getCreator().getId());
                statement.setString(2, object.getName());
                statement.executeUpdate();
                resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    object.setId(resultSet.getInt("id"));
                }
            } else {
                statement = connection.prepareStatement(SQL_UPDATE);
                statement.setString(1, object.getName());
                statement.setInt(2, object.getId());
                statement.executeUpdate();
            }
            return object;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, resultSet);
        }
    }

    @Override
    public void delete(@NonNull Integer id) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_DELETE);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, null);
        }
    }

    @Override
    public List<Room> findAll() {
        return null;
    }

    @Override
    public List<Room> findAll(@NonNull String token) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            List<Room> roomsByUser = new ArrayList<>();
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_ALL_BY_USER);
            statement.setString(1, token);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                roomsByUser.add(roomRowMapper.mapRow(resultSet));
            }
            return roomsByUser;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, resultSet);
        }
    }

    @Override
    public Room addUserInRoom(Integer userId, Integer roomId) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_ADD_USER_IN_ROOM);
            statement.setInt(1, roomId);
            statement.setInt(2, userId);
            statement.executeUpdate();
            return findById(roomId);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, null);
        }
    }

    @Override
    public Room removeUserFromRoom(Integer userId, Integer roomId) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_REMOVE_USER_FROM_ROOM);
            statement.setInt(1, roomId);
            statement.setInt(2, userId);
            statement.executeUpdate();
            return findById(roomId);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, null);
        }
    }

    @Override
    public Room findById(Integer id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            Room room = null;
            List<User> userInRoom = new ArrayList<>();
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_BY_ID);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                if (room == null) {
                    room = roomRowMapper.mapRow(resultSet);
                }
                userInRoom.add(userRowMapper.mapRow(resultSet));
            }
            Optional.ofNullable(room).ifPresent(r -> r.setUsers(userInRoom));
            return room;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, resultSet);
        }
    }
}
