package ru.itdrive.repository.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import ru.itdrive.models.File;
import ru.itdrive.models.User;
import ru.itdrive.repository.FileRepository;
import ru.itdrive.repository.RowMapper;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
@AllArgsConstructor
public class FileRepositoryImpl implements FileRepository {
    private static final String SQL_INSERT = "insert into user_file (file_name, local_path, user_id, content_type) values (?,?,?,?)";
    private static final String SQL_DELETE = "delete from user_file where id=?";
    private static final String SQL_SELECT_BY_USER = "select f.id as fId, f.file_name as fFileName, f.local_path as fFilePath, f.content_type as fContentType, " +
            "u.id as uId, u.login as uLogin " +
            "from user_file f " +
            " left join users u on f.user_id=u.id " +
            " where u.token = ? ";
    private static final String SQL_FIND_BY_ID = "select f.id as fId, f.file_name as fFileName, f.local_path as fFilePath, f.content_type as fContentType " +
            "from user_file f " +
            "where f.id = ?";
    private final RowMapper<File> fileUserRowMapper = row -> {
        User user = User.builder()
                .id(row.getInt("uId"))
                .login(row.getString("uLogin"))
                .build();
        return File.builder()
                .id(row.getInt("fId"))
                .fileName(row.getString("fFileName"))
                .localPath(row.getString("fFilePath"))
                .creator(user)
                .contentType(row.getString("fContentType"))
                .build();
    };
    private final RowMapper<File> fileRowMapper = row -> {
        return File.builder()
                .id(row.getInt("fId"))
                .fileName(row.getString("fFileName"))
                .localPath(row.getString("fFilePath"))
                .contentType(row.getString("fContentType"))
                .build();
    };
    private DataSource dataSource;

    @Override
    public File save(File object) {
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            if (object.getId() == null) {
                statement = connection.prepareStatement(SQL_INSERT);
                statement.setString(1, object.getFileName());
                statement.setString(2, object.getLocalPath());
                statement.setInt(3, object.getCreator().getId());
                statement.setString(4, object.getContentType());
                statement.executeUpdate();
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, null);
        }
    }

    @Override
    public void delete(Integer id) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_DELETE);
            statement.setInt(1, id);
            int countRows = statement.executeUpdate();
            if (countRows != 1) {
                throw new SQLException("Не удалось удалить файл");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, null);
        }
    }

    @Override
    public File findById(Integer id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_BY_ID);
            statement.setInt(1, id);
            result = statement.executeQuery();
            if (result.next()) {
                return fileRowMapper.mapRow(result);
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, result);
        }
    }

    @Override
    public List<File> findAll() {
        return null;
    }

    @Override
    public List<File> getFiles(String token) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_BY_USER);
            statement.setString(1, token);
            result = statement.executeQuery();
            List<File> lastFiles = new ArrayList<>();
            while (result.next()) {
                lastFiles.add(fileUserRowMapper.mapRow(result));
            }
            return lastFiles;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, result);
        }
    }
}
