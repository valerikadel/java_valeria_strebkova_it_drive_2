package ru.itdrive.repository;

import ru.itdrive.models.File;

import java.util.List;

public interface FileRepository extends CrudRepository<File> {
    List<File> getFiles(String token);

    File findById(Integer id);
}
