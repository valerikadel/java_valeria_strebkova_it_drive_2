package ru.itdrive.repository;

import java.util.List;

public interface CrudRepository<T> extends DatabaseClosing {
    T save(T object);

    void delete(Integer id);

    List<T> findAll();
}
