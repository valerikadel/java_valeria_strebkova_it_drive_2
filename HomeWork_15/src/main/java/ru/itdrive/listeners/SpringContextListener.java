package ru.itdrive.listeners;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.config.ApplicationConfig;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

@WebListener
public class SpringContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        Properties properties = new Properties();
        try {
            properties.load(new FileReader(servletContextEvent.getServletContext().getRealPath("/" + "\\roles.properties")));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        ServletContext servletContext = servletContextEvent.getServletContext();
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        servletContext.setAttribute("springContext", context);
        servletContext.setAttribute("properties", properties);
        servletContext.setAttribute("roles", properties);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
