package ru.itdrive.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class User {
    private Integer id;
    private String login;
    private String password;
    private Role role;
    private String token;
}
