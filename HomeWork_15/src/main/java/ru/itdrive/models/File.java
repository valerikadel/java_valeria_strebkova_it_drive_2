package ru.itdrive.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class File {
    private Integer id;
    private User creator;
    private String fileName;
    private String localPath;
    private String contentType;
}
