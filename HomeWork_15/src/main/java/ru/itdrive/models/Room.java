package ru.itdrive.models;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Room {
    private Integer id;
    private String name;
    private User creator;
    private List<User> users;
}
