package ru.itdrive.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

@Configuration
@ComponentScan(basePackages = "ru.itdrive")
@PropertySource("classpath:application:properties")
public class ApplicationConfig {

    @Autowired
    private Environment environment;

    @Bean
    public HikariConfig hikariConfig() {
        HikariConfig config = new HikariConfig();
        hikariConfig().setDriverClassName(environment.getProperty("db.driver.name"));
        hikariConfig().setPassword(environment.getProperty("db.password"));
        hikariConfig().setUsername(environment.getProperty("db.user"));
        hikariConfig().setJdbcUrl(environment.getProperty("db.url"));
        return config;
    }

    @Bean
    public DataSource dataSource() {
        return new HikariDataSource(hikariConfig());
    }
}
