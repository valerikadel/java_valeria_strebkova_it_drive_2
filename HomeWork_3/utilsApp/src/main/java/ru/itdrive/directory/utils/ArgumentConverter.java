package ru.itdrive.directory.utils;

import com.beust.jcommander.IStringConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArgumentConverter implements IStringConverter<List<String>> {
    @Override
    public List<String> convert(String files) {
        String[] paths = files.split(",");
        return new ArrayList<>(Arrays.asList(paths));
    }
}
