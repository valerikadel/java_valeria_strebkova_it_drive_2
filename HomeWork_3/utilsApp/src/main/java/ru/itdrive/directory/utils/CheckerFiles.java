package ru.itdrive.directory.utils;

import ru.itdrive.directory.app.FileInformation;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CheckerFiles {
    public static List<FileInformation> getFromDirectory(String path) throws IOException {
        Path p = Paths.get(path);
        List<FileInformation> files = new ArrayList<>();
        if (Files.exists(p)) {
            if (Files.isDirectory(p, LinkOption.NOFOLLOW_LINKS)) {
                Files.list(p).forEach(file ->
                {
                    long size = getSize(file.toFile());
                    files.add(new FileInformation(size, file.getFileName().toString()));
                });
            } else {
                System.out.println("Не является каталогом");
            }
        } else {
            System.out.println("Каталога не существует");
        }
        return files;
    }

    private static long getSize(File file) {
        long size;
        if (file.isDirectory() && file.listFiles() != null) {
            size = 0;
            for (File child : Objects.requireNonNull(file.listFiles())) {
                size += getSize(child);
            }
        } else {
            size = file.length();
        }
        return size;
    }
}