package ru.itdrive.directory.utils;

import ru.itdrive.directory.app.FileInformation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DirectoryWorking extends Thread {
    private final String path;
    List<FileInformation> list;

    public DirectoryWorking(String path) {
        this.path = path;
        list = new ArrayList<>();
    }

    public List<FileInformation> getList() {
        return this.list;
    }

    @Override
    public void run() {
        try {
            System.out.println(path + ": started ");
            list = CheckerFiles.getFromDirectory(this.path);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

}
