package ru.itdrive.directory.app;

import com.beust.jcommander.JCommander;
import ru.itdrive.directory.utils.DirectoryWorking;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Program {

    public static void main(String[] args) throws IOException {
        FilePath arguments = new FilePath();

        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(String.join(" ", args));

        Map<String, List<FileInformation>> result = new HashMap<>();
        arguments.path.forEach(p -> {
            DirectoryWorking working = new DirectoryWorking(p);
            working.start();
            try {
                working.join();
                result.put(p, working.getList());
            } catch (InterruptedException e) {
            }
        });
        printResult(result);
    }

    public static void printResult(Map<String, List<FileInformation>> result) {
        result.forEach((d, f) -> {
            System.out.println("Directory: " + d);
            f.forEach(file -> {
                System.out.println(String.format("\t File:%s, size:%s bytes", file.getFileName(), file.getSize()));
            });
        });
    }
}