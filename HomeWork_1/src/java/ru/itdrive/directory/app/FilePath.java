package ru.itdrive.directory.app;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.util.ArrayList;

@Parameters(separators = "=")
public class FilePath {
    @Parameter()
    public ArrayList<String> partOfPath;

    @Parameter(names = "--path")
    public String mainPartOfPath;
}