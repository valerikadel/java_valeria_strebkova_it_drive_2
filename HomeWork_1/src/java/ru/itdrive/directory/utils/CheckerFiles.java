package ru.itdrive.directory.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class CheckerFiles {
    public void printFromDirectory(String path) throws IOException {
        Path p = Paths.get(path);
        System.out.println(path);
        if (Files.exists(p)) {
            if (Files.isDirectory(p, LinkOption.NOFOLLOW_LINKS)) {
                Files.list(p).forEach(file ->
                {
                    long size = getSize(file.toFile());
                    System.out.println(String.format("File:%s, size: %s bytes", file.getFileName().toString(),
                            size));
                });
            } else {
                System.out.println("Не является каталогом");
            }
        } else {
            System.out.println("Каталога не существует");
        }

    }

    public static long getSize(File file) {
        long size;
        if (file.isDirectory() && file.listFiles() != null) {
            size = 0;
            for (File child : Objects.requireNonNull(file.listFiles())) {
                size += getSize(child);
            }
        } else {
            size = file.length();
        }
        return size;
    }
}