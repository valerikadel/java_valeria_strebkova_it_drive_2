package ru.itdrive.client;

import arguments.RoomArgument;
import arguments.SocketArgument;
import com.beust.jcommander.JCommander;

import java.util.MissingFormatArgumentException;
import java.util.Optional;
import java.util.Scanner;

public class Main {
    private static final int PORT= 7777;
    private static final String HOST="localhost";
    public static void main(String[] args) {

        try {
            System.out.println("Введите свой ник");
            Scanner scanner = new Scanner(System.in);

            ClientSocket clientSocket = new ClientSocket(HOST, PORT);
            while (true) {
                String message = scanner.nextLine();
                clientSocket.sendToServer(message);
            }
        } catch (NumberFormatException e) {
            System.out.println("Указанный порт не является числом");
        }
    }
}
