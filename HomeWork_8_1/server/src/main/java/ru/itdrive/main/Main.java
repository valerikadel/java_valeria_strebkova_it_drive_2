package ru.itdrive.main;

import ru.itdrive.server.ServerSocket;

public class Main {
    private static final int PORT= 7777;
    private static final String HOST="localhost";
    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket();
            serverSocket.start(PORT);
        } catch (NumberFormatException e) {
            System.out.println("Указанный порт не является числом");
        }
    }
}
