package ru.itdrive.server;

import ru.itdrive.chat.CharServiceImpl;
import ru.itdrive.chat.ChatService;
import ru.itdrive.chat.TypeCommand;
import ru.itdrive.db.model.User;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import static ru.itdrive.chat.TypeCommand.*;


public class ServerSocket {
    private List<ClientRunnable> clients;


    public ServerSocket() {
        this.clients = new CopyOnWriteArrayList<>();
    }

    public void start(int port) {
        java.net.ServerSocket server;
        try {
            server = new java.net.ServerSocket(port);
            while (true) {
                ClientRunnable run = new ClientRunnable(server.accept(), server);
                clients.add(run);
                run.start();
            }

        } catch (IOException | SQLException | ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public class ClientRunnable extends Thread {
        private final String COMMAND_CHOOSE_ROOM = "choose room";
        private final String COMMAND_EXIT_ROOM = "exit room";
        private ChatService service;
        private java.net.ServerSocket serverSocket;
        private Socket client;
        private PrintWriter printWriter;
        private User currentUser;
        private TypeCommand typeCommand;

        public ClientRunnable(Socket client, java.net.ServerSocket serverSocket) throws SQLException, ClassNotFoundException {
            this.client = client;
            this.serverSocket = serverSocket;
        }

        public User getCurrentUser() {
            return currentUser;
        }

        protected PrintWriter getPrintWriter() {
            return this.printWriter;
        }

        public void run() {
            try {
                String encoding = System.getProperty("console.encoding", "utf-8"); //пробы кодировки
                InputStream clientInputStream = client.getInputStream();
                BufferedReader clientReader = new BufferedReader(new InputStreamReader(clientInputStream, encoding));
                printWriter = new PrintWriter(client.getOutputStream(), true, Charset.forName(encoding));
                service = new CharServiceImpl();
                while (true) {
                    String inputLine = clientReader.readLine();
                    if (inputLine != null) {
                        String commandLine = parseCommand(inputLine);
                        if (currentUser == null) {
                            currentUser = service.start(inputLine);
                            if (this.currentUser.getCurrentRoom() != null && this.currentUser.getCurrentRoom().getId() != 0) {
                                printWriter.println(service.printLastMessages());
                            } else {
                                printWriter.println(service.printRooms());
                            }
                        } else {
                            switch (typeCommand) {
                                case MESSAGE:
                                    Optional.ofNullable(service.sendMessage(commandLine)).ifPresent(this::sendAll);
                                    break;
                                case EXIT_ROOM:
                                    printWriter.println(service.exitRoom());
                                    break;
                                case CHOOSE_ROOM:
                                    printWriter.println(service.chooseRoom(commandLine));
                                    break;
                                default:
                                    this.printWriter.println("Неизвестная команда");
                            }
                        }
                    }
                }
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        }

        /**
         * Пока не придумала,как получше
         * Событие для отправки сообщений всем пользователям комнаты
         */
        private void sendAll(String message) {
            clients.stream().filter(cl -> cl.getCurrentUser() != null
                    && Optional.ofNullable(cl.getCurrentUser().getCurrentRoom())
                    .filter(r -> r.getId() != 0)
                    .map(r -> r.getId().equals(currentUser.getCurrentRoom().getId()))
                    .orElse(false)).map(ServerSocket.ClientRunnable::getPrintWriter).forEach(writer -> {
                if (writer != null) {
                    writer.println(message);
                }
            });
        }

        /**
         * Парсим строку и понимаем, что пользователь ввел и чего он пытается добиться
         *
         * @param inputLine входящая строка с клиента
         */
        private String parseCommand(String inputLine) {
            String commandLine = null;
            typeCommand = inputLine.toLowerCase().equals(COMMAND_EXIT_ROOM) ? EXIT_ROOM : null;
            if (typeCommand == null && inputLine.length() > COMMAND_CHOOSE_ROOM.length()
                    && inputLine.substring(0, COMMAND_CHOOSE_ROOM.length()).trim().toLowerCase().equals(COMMAND_CHOOSE_ROOM)) {
                typeCommand = CHOOSE_ROOM;
                commandLine = inputLine.substring(COMMAND_CHOOSE_ROOM.length());
            }
            if (typeCommand == null || inputLine.equals(commandLine)) {
                typeCommand = MESSAGE;
                commandLine = inputLine;
            }
            return commandLine;
        }
    }
}
