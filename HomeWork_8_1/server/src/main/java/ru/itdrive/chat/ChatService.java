package ru.itdrive.chat;

import ru.itdrive.db.model.User;

import java.sql.SQLException;

public interface ChatService {
    String exitRoom() throws SQLException, ClassNotFoundException;

    String chooseRoom(String nameRoom) throws SQLException, ClassNotFoundException;

    String sendMessage(String message) throws SQLException, ClassNotFoundException;

    User start(String nickName) throws SQLException, ClassNotFoundException;

    String printLastMessages();

    String printRooms();
}
