package ru.itdrive.chat;

import ru.itdrive.db.model.Message;
import ru.itdrive.db.model.Room;
import ru.itdrive.db.model.User;
import ru.itdrive.db.repository.MessageRepository;
import ru.itdrive.db.repository.RoomRepository;
import ru.itdrive.db.repository.UserRepository;
import ru.itdrive.db.repository.impl.MessageRepositoryImpl;
import ru.itdrive.db.repository.impl.RoomRepositoryImpl;
import ru.itdrive.db.repository.impl.UserRepositoryImpl;

import java.sql.Connection;
import java.sql.SQLException;

import static java.sql.DriverManager.getConnection;

public class CharServiceImpl implements ChatService {
    protected static final String DB_URL = "jdbc:postgresql://localhost:5432/chat";
    protected static final String DB_USER = "postgres";
    protected static final String DB_PASSWORD = "admin";
    private UserRepository userRepository;
    private RoomRepository roomRepository;
    private MessageRepository messageRepository;
    private Connection connection;
    private User currentUser;

    public CharServiceImpl() {
    }

    private void createConnection() {
        try {
            Class.forName("org.postgresql.Driver");
            this.connection = getConnection(DB_URL, DB_USER, DB_PASSWORD);
            this.userRepository = new UserRepositoryImpl(connection);
            this.roomRepository = new RoomRepositoryImpl(connection);
            this.messageRepository = new MessageRepositoryImpl(connection);
        } catch (SQLException | ClassNotFoundException e) {
            throw new IllegalArgumentException("Что-то явно пошло не так" + e);
        }
    }

    @Override
    public String exitRoom() {
        try {
            createConnection();
            String result = null;
            if (currentUser == null) {
                result = "Пользователь не определен";
            } else if (currentUser.getCurrentRoom() == null || currentUser.getCurrentRoom().getId() == 0) {
                result = "Текущая комната не определена";
            } else {
                currentUser = userRepository.exitFromRoom(currentUser.getId());
                result = printRooms();
            }
            this.connection.close();
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException("Что-то явно пошло не так" + e);
        }
    }

    @Override
    public String chooseRoom(String nameRoom) {
        try {
            createConnection();
            if (nameRoom != null) {
                if (currentUser == null) {
                    return "Пользователь не определен";
                } else if (currentUser.getCurrentRoom() != null && currentUser.getCurrentRoom().getId() != 0) {
                    return "Сначала выйдите из текущей комнаты - -exit from room";
                } else {
                    Room room = roomRepository.findByName(nameRoom);
                    currentUser.setCurrentRoom(room);
                    currentUser = userRepository.save(currentUser);
                    printLastMessages();
                }
            }
            String rooms = printRooms();
            this.connection.close();
            return rooms;
        } catch (SQLException e) {
            throw new IllegalArgumentException("Что-то явно пошло не так" + e);
        }

    }

    @Override
    public String sendMessage(String message) {
        try {
            createConnection();
            if (message != null) {
                if (this.currentUser.getCurrentRoom() != null && this.currentUser.getCurrentRoom().getId() != 0) {
                    return messageRepository.save(new Message(message, this.currentUser, this.currentUser.getCurrentRoom())).toString();
                } else {
                    return printRooms();
                }
            }
            this.connection.close();
            return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException("Что-то явно пошло не так" + e);
        }
    }

    @Override
    public User start(String nickName) {
        try {
            createConnection();
            currentUser = userRepository.findByNickName(nickName);
            if (currentUser == null) {
                currentUser = userRepository.save(new User(nickName, true));
            }
            this.connection.close();
            return currentUser;
        } catch (SQLException e) {
            throw new IllegalArgumentException("Что-то явно пошло не так" + e);
        }
    }

    @Override
    public String printRooms() {
        return ("Укажите комнату: -choose room <имя комнаты> \n" +
                roomRepository.findAll().stream().map(Room::toString).reduce("", (prev, next) -> (prev + "\n" + next)));
    }

    @Override
    public String printLastMessages() {
        return (messageRepository.findLastMessages(currentUser.getCurrentRoom().getId()).stream()
                .map(Message::toString)
                .reduce("", (prev, next) -> (prev + "\n" + next)));
    }
}
