package ru.itdrive.db.repository;

import java.util.List;

public interface CrudRepository<T> {
    T save(T object);

    void delete(Integer id);

    List<T> findAll();
}
