package ru.itdrive.db.repository.impl;

import ru.itdrive.db.model.Room;
import ru.itdrive.db.repository.RoomRepository;
import ru.itdrive.db.repository.RowMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RoomRepositoryImpl implements RoomRepository {

    private static final String SQL_INSERT = "insert into room (name) values (?)";
    private static final String SQL_DELETE = "delete from room where id=?";
    private static final String SQL_FIND_ALL = "select r.id as rId, r.name as rName from room r";
    private static final String SQL_FIND_BY_NAME = "select r.id as rId, r.name as rName from room r where lower(r.name) like ? ";

    private Connection connection;
    private RowMapper<Room> roomRowMapper = new RowMapper<Room>() {
        @Override
        public Room mapRow(ResultSet row) throws SQLException {
            return new Room(row.getInt("rId"),
                    row.getString("rName"));
        }
    };

    public RoomRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Room save(Room object) {
        try {
            if (object.getId() == null) {
                PreparedStatement statement = connection.prepareStatement(SQL_INSERT);
                statement.setString(1, object.getName());
                statement.executeUpdate();
                statement.close();
            }
            return object;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE);
            statement.setInt(1, id);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Room> findAll() {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL);
            ResultSet resultSet = statement.executeQuery();
            List<Room> rooms = new ArrayList<>();
            while (resultSet.next()) {
                rooms.add(roomRowMapper.mapRow(resultSet));
            }
            statement.close();
            return rooms;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Room findByName(String name) {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NAME);
            statement.setString(1, "%" + name.trim().toLowerCase() + "%");
            System.out.println(statement.toString());
            ResultSet resultSet = statement.executeQuery();
            Room room = null;
            if (resultSet.next()) {
                room = roomRowMapper.mapRow(resultSet);
            }
            statement.close();
            return room;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
