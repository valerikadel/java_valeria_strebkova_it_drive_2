package ru.itdrive.db.repository;

import ru.itdrive.db.model.Room;

public interface RoomRepository extends CrudRepository<Room> {
    Room findByName(String name);
}
