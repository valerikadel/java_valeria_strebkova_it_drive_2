package ru.itdrive.db.repository;

import ru.itdrive.db.model.User;

public interface UserRepository extends CrudRepository<User> {
    User findByNickName(String name);

    User exitFromRoom(Integer id);

    User findById(Integer id);
}
