package ru.itdrive.db.model;

import java.util.Set;

public class User {
    private Integer id;
    private String nickname;
    private Room currentRoom;
    private Boolean isActive;
    private Set<Message> messages;

    public User(Integer id, String nickname, Room room, Boolean isActive) {
        this.id = id;
        this.nickname = nickname;
        this.currentRoom = room;
        this.isActive = isActive;
    }

    public User(String nickname, Boolean isActive) {
        this.nickname = nickname;
        this.isActive = isActive;
    }

    public Integer getId() {
        return id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Room getCurrentRoom() {
        if (currentRoom.getId() == 0) {
            return null;
        }
        return currentRoom;
    }

    public void setCurrentRoom(Room currentRoom) {
        this.currentRoom = currentRoom;
    }

    public Set<Message> getMessages() {
        return messages;
    }

    public void setMessages(Set<Message> messages) {
        this.messages = messages;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }
}
