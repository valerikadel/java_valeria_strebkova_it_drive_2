package ru.itdrive.db.repository.impl;

import ru.itdrive.db.model.Message;
import ru.itdrive.db.model.User;
import ru.itdrive.db.repository.MessageRepository;
import ru.itdrive.db.repository.RowMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MessageRepositoryImpl implements MessageRepository {
    private static final String SQL_INSERT = "insert into message (creation_date, creator_id,room_id, message) values (now(),?,?,?)";
    private static final String SQL_DELETE = "delete from message where id=?";
    private static final String SQL_SELECT_LAST = "select m.id as mId, m.creation_date as creationDate, m.message as message, u.nickname as creator " +
            "from message m " +
            " left join chat_user u on m.creator_id=u.id " +
            " where m.room_id=? order by m.creation_date desc limit 30";
    private static final String SQL_SELECT_LAST_SENT = "select m.id as mId, m.creation_date as creationDate, m.message as message, u.nickname as creator " +
            "from message m " +
            " left join chat_user u on m.creator_id=u.id " +
            " where m.room_id=? and lower(m.message) like ? order by m.creation_date desc limit 1";
    private Connection connection;

    private RowMapper<Message> messageRowMapper = new RowMapper<Message>() {
        @Override
        public Message mapRow(ResultSet row) throws SQLException {
            return new Message(row.getInt("mId"),
                    row.getTimestamp("creationDate"),
                    row.getString("message"),
                    new User(row.getString("creator"), true));
        }
    };

    public MessageRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Message save(Message object) {
        try {
            if (object.getId() == null) {
                PreparedStatement statement = connection.prepareStatement(SQL_INSERT);
                statement.setInt(1, object.getCreator().getId());
                statement.setInt(2, object.getRoom().getId());
                statement.setString(3, object.getMessage());
                statement.executeUpdate();
                statement.close();
            }
            return findLastSent(object.getRoom().getId(), object.getMessage());
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE);
            statement.setInt(1, id);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Message> findAll() {
        return null;
    }

    @Override
    public List<Message> findLastMessages(int roomId) {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_LAST);
            statement.setInt(1, roomId);
            ResultSet result = statement.executeQuery();
            List<Message> lastMessages = new ArrayList<>();
            while (result.next()) {
                lastMessages.add(messageRowMapper.mapRow(result));
            }
            statement.close();
            return lastMessages.stream().sorted(Comparator.comparing(Message::getCreationDate)).collect(Collectors.toList());
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Message findLastSent(Integer roomId, String text) {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_LAST_SENT);
            statement.setInt(1, roomId);
            statement.setString(2, "%" + text.toLowerCase() + "%");
            ResultSet result = statement.executeQuery();
            Message lastMessage = null;
            if (result.next()) {
                lastMessage = messageRowMapper.mapRow(result);
            }
            statement.close();
            return lastMessage;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
