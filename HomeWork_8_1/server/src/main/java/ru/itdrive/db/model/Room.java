package ru.itdrive.db.model;

import java.util.Set;

public class Room {
    private Integer id;
    private String name;
    private Set<User> users;
    private Set<Message> messages;

    public Room(Integer id,String name) {
        this.name = name;
        this.id=id;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return
                "id=" + id +
                        ", name='" + name;
    }
}
