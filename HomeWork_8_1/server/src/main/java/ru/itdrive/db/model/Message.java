package ru.itdrive.db.model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Message {
    private Integer id;
    private Timestamp creationDate;
    private User creator;
    private Room room;
    private String message;

    public Message(Integer id, Timestamp creationDate, String message, User creator) {
        this.id = id;
        this.creationDate = creationDate;
        this.message = message;
        this.creator = creator;
    }

    public Message(String message, User user, Room room) {
        this.message = message;
        this.creator = user;
        this.room = room;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "from:" + creator.getNickname().trim() +
                ", creationDate=" + new SimpleDateFormat().format(creationDate) +
                "\n" + message.trim();
    }
}
