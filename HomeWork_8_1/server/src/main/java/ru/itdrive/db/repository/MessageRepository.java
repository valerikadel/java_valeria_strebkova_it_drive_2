package ru.itdrive.db.repository;

import ru.itdrive.db.model.Message;

import java.util.List;

public interface MessageRepository extends CrudRepository<Message> {
    List<Message> findLastMessages(int roomId);

    Message findLastSent(Integer roomId, String text);
}
