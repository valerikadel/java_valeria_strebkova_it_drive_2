package ru.itdrive.db.repository.impl;

import ru.itdrive.db.model.Room;
import ru.itdrive.db.model.User;
import ru.itdrive.db.repository.RowMapper;
import ru.itdrive.db.repository.UserRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UserRepositoryImpl implements UserRepository {
    private static final String SQL_INSERT = "insert into chat_user (nickname, is_active) values (?,?)";
    private static final String SQL_DELETE = "update chat_user set is_active=false where id=?";
    //language=SQL
    private static final String SQL_FIND_BY_NAME = "select u.id as uId, u.nickname as uName, u.is_active as uIsActive, r.id as rId, r.name as rName " +
            "from chat_user u " +
            "left join room r on u.current_room_id=r.id where lower(u.nickname) like ?";
    //language=SQL
    private static final String SQL_FIND_BY_ID = "select u.id as uId, u.nickname as uName, u.is_active as uIsActive,r.id as rId, r.name as rName " +
            "from chat_user u " +
            "left join room r on u.current_room_id=r.id where u.id = ?";
    //language=SQL
    private static final String SQL_UPDATE_CURRENT_ROOM = "update chat_user set current_room_id =? where id=?";
    private static final String SQL_SET_NULL_CURRENT_ROOM = "update chat_user set current_room_id = null where id=?";
    private Connection connection;
    private RowMapper<User> userRowMapper = new RowMapper<User>() {
        @Override
        public User mapRow(ResultSet row) throws SQLException {
            return new User(row.getInt("uId"),
                    row.getString("uName"),
                    new Room(row.getInt("rId"),
                            row.getString("rName")),
                    row.getBoolean("uIsActive"));
        }
    };

    public UserRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public User save(User object) {
        try {
            if (object.getId() == null) {
                PreparedStatement statement = connection.prepareStatement(SQL_INSERT);
                statement.setString(1, object.getNickname());
                statement.setBoolean(2, object.getActive());
                statement.executeUpdate();
                statement.close();
            } else {
                User old = findById(object.getId());
                if (old.getCurrentRoom() == null || !old.getCurrentRoom().getId().equals(object.getCurrentRoom().getId())) {
                    PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_CURRENT_ROOM);
                    statement.setInt(1, object.getCurrentRoom().getId());
                    statement.setInt(2, object.getId());
                    statement.executeUpdate();
                    statement.close();
                }
            }
            return findByNickName(object.getNickname());
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE);
            statement.setInt(1, id);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public User findByNickName(String name) {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NAME);
            statement.setString(1, "%" + name.trim().toLowerCase() + "%");
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                User user = userRowMapper.mapRow(resultSet);
                statement.close();
                return user;
            }
            statement.close();
            return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public User findById(Integer id) {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            User user = userRowMapper.mapRow(resultSet);
            statement.close();
            return user;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public User exitFromRoom(Integer id) {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_SET_NULL_CURRENT_ROOM);
            statement.setInt(1, id);
            statement.executeUpdate();
            statement.close();
            return findById(id);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
