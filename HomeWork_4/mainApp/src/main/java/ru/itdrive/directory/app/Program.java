package ru.itdrive.directory.app;

import com.beust.jcommander.JCommander;
import pool.ThreadPool;
import ru.itdrive.directory.utils.CollectorResult;
import ru.itdrive.directory.utils.DirectoryWorking;

import java.io.IOException;

class Program {

    public static void main(String[] args) throws IOException {
        FilePath arguments = new FilePath();

        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(String.join(" ", args));

        ThreadPool pool = new ThreadPool(3, arguments.path.size());

        CollectorResult parent = new CollectorResult(arguments.path.size());

        arguments.path.forEach(p -> {
            pool.submit(new DirectoryWorking(p, parent));
        });
    }
}