package pool;

import ru.itdrive.directory.utils.DirectoryWorking;

import java.util.Deque;
import java.util.LinkedList;

public class ThreadPool {
    private Deque<Runnable> tasks;
    private Worker threads[];

    public ThreadPool(int threadCount, int countAll) {
        try {
            this.tasks = new LinkedList<>();
            this.threads = new Worker[threadCount];
            for (int i = 0; i < threadCount; i++) {
                this.threads[i] = new Worker();
                this.threads[i].start();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void submit(DirectoryWorking task) {
        synchronized (tasks) {
            tasks.addLast(task);
            tasks.notify();
        }
    }

    private class Worker extends Thread {

        @Override
        public void run() {
            try {
                //ну бесконечно запущенные потоки
                while (true) {
                    Runnable task = null;
                    synchronized (tasks) {
                        //Пока задач нет - поток ожидает
                        while (tasks.isEmpty()) {
                            tasks.wait();
                        }
                        //Вытаскиваем задачу и выполняем ее
                        task = tasks.pop();
                        tasks.notify();
                    }
                    task.run();

                    //Вывод инф сообщения о том,что поток свободен
                    System.out.println(Thread.currentThread().getName() + " free");
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
