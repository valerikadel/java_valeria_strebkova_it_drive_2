package ru.itdrive.directory.utils;

import ru.itdrive.directory.app.FileInformation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DirectoryWorking implements Runnable {
    private final String path;
    List<FileInformation> list;
    private CollectorResult parent;

    public DirectoryWorking(String path, CollectorResult parent) {
        this.parent = parent;
        this.path = path;
        list = new ArrayList<>();
    }

    public List<FileInformation> getList() {
        return this.list;
    }

    public String getPath() {
        return this.path;
    }

    @Override
    public void run() {
        try {
            System.out.println(Thread.currentThread().getName() + " started " + path);
            list = CheckerFiles.getFromDirectory(this.path);
            //Записываем результат выполнения в мапу
            synchronized (parent) {
                parent.getResult().put(this.path, list);
                parent.setCountDone(parent.getCountDone() + 1);
            }
            //Если исходное кол-во задач равно сделанным, то
            //нужно вывести результат в отдельном потоке
            if (parent.getCountAll() == parent.getCountDone()) {
                parent.getPrinter().start();
            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
