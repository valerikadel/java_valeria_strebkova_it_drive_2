package ru.itdrive.directory.app;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import ru.itdrive.directory.utils.ArgumentConverter;

import java.util.List;

@Parameters(separators = "=")
public class FilePath {
    @Parameter(names = "--path", listConverter = ArgumentConverter.class)
    public List<String> path;
}