package ru.itdrive.directory.utils;

import ru.itdrive.directory.app.FileInformation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CollectorResult {
    Map<String, List<FileInformation>> result; //мапа с результатом
    private PrinterWorker printer; //поток для вывода
    private volatile int countDone; //кол-во сделанных задач
    private int countAll; //все задачи

    public CollectorResult(int countAll) {
        this.countAll = countAll;
        this.printer = new PrinterWorker();
        this.result = new HashMap<>();
    }

    public void setCountDone(int countDone) {
        this.countDone = countDone;
    }

    public Map<String, List<FileInformation>> getResult() {
        return result;
    }

    public PrinterWorker getPrinter() {
        return printer;
    }

    public int getCountDone() {
        return countDone;
    }

    public int getCountAll() {
        return countAll;
    }

    public class PrinterWorker extends Thread {

        @Override
        public void run() {
            result.forEach((d, f) -> {
                System.out.println("Directory: " + d);
                f.forEach(file -> {
                    System.out.println(String.format("\t File:%s, size:%s bytes", file.getFileName(), file.getSize()));
                });
            });
        }
    }
}
