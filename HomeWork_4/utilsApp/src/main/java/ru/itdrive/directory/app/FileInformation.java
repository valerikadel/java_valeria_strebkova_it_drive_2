package ru.itdrive.directory.app;

public class FileInformation {

    private long size;
    private String fileName;

    public long getSize() {
        return size;
    }

    public String getFileName() {
        return fileName;
    }

    public FileInformation(long size, String fileName) {
        this.size = size;
        this.fileName = fileName;
    }
}
