package ru.itdrive.web.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.chat.RoomService;
import ru.itdrive.config.ApplicationConfig;
import ru.itdrive.db.model.Room;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/createRoom")
public class CreateRoomServlet extends HttpServlet {
    private RoomService roomService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("html/createRoom.html");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        Integer maxCapacity = Integer.valueOf(req.getParameter("maxCount"));
        roomService.save(Room.builder().maxCapacity(maxCapacity).name(name).build());
    }

    @Override
    public void init() throws ServletException {
        ApplicationContext config = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        roomService = config.getBean(RoomService.class);
    }
}
