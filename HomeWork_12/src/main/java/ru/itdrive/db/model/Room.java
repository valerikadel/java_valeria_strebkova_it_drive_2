package ru.itdrive.db.model;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class Room {
    private Integer id;
    private String name;
    private Set<User> users;
    private Set<Message> messages;
    private Integer maxCapacity;

    @Override
    public String toString() {
        return
                "id=" + id +
                        ", name='" + name;
    }
}
