package ru.itdrive.db.model;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class User {
    private Integer id;
    private String nickname;
    private Room currentRoom;
    private Boolean isActive;
    private Set<Message> messages;

    public Room getCurrentRoom() {
        if (currentRoom.getId() == 0) {
            return null;
        }
        return currentRoom;
    }
}
