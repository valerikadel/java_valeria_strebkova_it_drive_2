package ru.itdrive.db.repository.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import ru.itdrive.db.model.Message;
import ru.itdrive.db.model.User;
import ru.itdrive.db.repository.DBClosing;
import ru.itdrive.db.repository.MessageRepository;
import ru.itdrive.db.repository.RowMapper;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


@Component
@AllArgsConstructor
public class MessageRepositoryImpl extends DBClosing implements MessageRepository {
    private static final String SQL_INSERT = "insert into message (creation_date, creator_id,room_id, message) values (now(),?,?,?)";
    private static final String SQL_DELETE = "delete from message where id=?";
    private static final String SQL_SELECT_LAST = "select m.id as mId, m.creation_date as creationDate, m.message as message, u.nickname as creator " +
            "from message m " +
            " left join chat_user u on m.creator_id=u.id " +
            " where m.room_id=? order by m.creation_date desc limit 30";
    private static final String SQL_SELECT_LAST_SENT = "select m.id as mId, m.creation_date as creationDate, m.message as message, u.nickname as creator " +
            "from message m " +
            " left join chat_user u on m.creator_id=u.id " +
            " where m.room_id=? and lower(m.message) like ? order by m.creation_date desc limit 1";
    private final RowMapper<Message> messageRowMapper = row -> {
        User user = User.builder()
                .nickname(row.getString("creator"))
                .isActive(true)
                .build();
        return Message.builder()
                .id(row.getInt("mId"))
                .creationDate(row.getTimestamp("creationDate"))
                .message(row.getString("message"))
                .creator(user)
                .build();
    };
    private DataSource dataSource;

    @Override
    public Message save(Message object) {
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            if (object.getId() == null) {
                statement = connection.prepareStatement(SQL_INSERT);
                statement.setInt(1, object.getCreator().getId());
                statement.setInt(2, object.getRoom().getId());
                statement.setString(3, object.getMessage());
                statement.executeUpdate();
            }
            return findLastSent(object.getRoom().getId(), object.getMessage());
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, null);
        }
    }

    @Override
    public void delete(Integer id) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_DELETE);
            statement.setInt(1, id);
            int countRows = statement.executeUpdate();
            if (countRows != 1) {
                throw new SQLException("Не удалось удалить сообщение");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, null);
        }
    }

    @Override
    public List<Message> findAll() {
        return null;
    }

    @Override
    public List<Message> findLastMessages(int roomId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_LAST);
            statement.setInt(1, roomId);
            result = statement.executeQuery();
            List<Message> lastMessages = new ArrayList<>();
            while (result.next()) {
                lastMessages.add(messageRowMapper.mapRow(result));
            }
            return lastMessages.stream().sorted(Comparator.comparing(Message::getCreationDate)).collect(Collectors.toList());
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, result);
        }
    }

    @Override
    public Message findLastSent(Integer roomId, String text) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_LAST_SENT);
            statement.setInt(1, roomId);
            statement.setString(2, "%" + text.toLowerCase() + "%");
            result = statement.executeQuery();
            Message lastMessage = null;
            if (result.next()) {
                lastMessage = messageRowMapper.mapRow(result);
            }
            return lastMessage;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, result);
        }
    }
}
