package ru.itdrive.db.repository.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import ru.itdrive.db.model.Room;
import ru.itdrive.db.model.User;
import ru.itdrive.db.repository.DBClosing;
import ru.itdrive.db.repository.RowMapper;
import ru.itdrive.db.repository.UserRepository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;


@Component
@AllArgsConstructor
public class UserRepositoryImpl extends DBClosing implements UserRepository {
    private static final String SQL_INSERT = "insert into chat_user (nickname, is_active) values (?,?)";
    private static final String SQL_DELETE = "update chat_user set is_active=false where id=?";
    //language=SQL
    private static final String SQL_FIND_BY_NAME = "select u.id as uId, u.nickname as uName, u.is_active as uIsActive, r.id as rId, r.name as rName " +
            "from chat_user u " +
            "left join room r on u.current_room_id=r.id where lower(u.nickname) like ?";
    //language=SQL
    private static final String SQL_FIND_BY_ID = "select u.id as uId, u.nickname as uName, u.is_active as uIsActive,r.id as rId, r.name as rName " +
            "from chat_user u " +
            "left join room r on u.current_room_id=r.id where u.id = ?";
    //language=SQL
    private static final String SQL_UPDATE_CURRENT_ROOM = "update chat_user set current_room_id =? where id=?";
    private static final String SQL_SET_NULL_CURRENT_ROOM = "update chat_user set current_room_id = null where id=?";
    private final RowMapper<User> userRowMapper = row -> {
        Room room = Room.builder()
                .id(row.getInt("rId"))
                .name(row.getString("rName"))
                .build();
        return User.builder()
                .id(row.getInt("uId"))
                .nickname(row.getString("uName"))
                .isActive(row.getBoolean("uIsActive"))
                .currentRoom(room)
                .build();
    };
    private DataSource dataSource;

    @Override
    public User save(User object) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            connection = dataSource.getConnection();
            if (object.getId() == null) {
                statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, object.getNickname());
                statement.setBoolean(2, object.getIsActive());
                statement.executeUpdate();
                result = statement.getGeneratedKeys();
                if (result.next()) {
                    object.setId(result.getInt("id"));
                }
            } else {
                User old = findById(object.getId());
                if (old.getCurrentRoom() == null || !old.getCurrentRoom().getId().equals(object.getCurrentRoom().getId())) {
                    statement = connection.prepareStatement(SQL_UPDATE_CURRENT_ROOM);
                    statement.setInt(1, object.getCurrentRoom().getId());
                    statement.setInt(2, object.getId());
                    statement.executeUpdate();
                }
            }
            return findByNickName(object.getNickname());
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, result);
        }
    }

    @Override
    public void delete(Integer id) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_DELETE);
            statement.setInt(1, id);
            int countRows = statement.executeUpdate();
            if (countRows != 1) {
                throw new SQLException("Не удалось удалить пользователя");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, null);
        }
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public User findByNickName(String name) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_BY_NAME);
            statement.setString(1, "%" + name.trim().toLowerCase() + "%");
            result = statement.executeQuery();
            if (result.next()) {
                return userRowMapper.mapRow(result);
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, result);
        }
    }

    @Override
    public User findById(Integer id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_BY_ID);
            statement.setInt(1, id);
            result = statement.executeQuery();
            if (result.next()) {
                return userRowMapper.mapRow(result);
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, result);
        }
    }

    @Override
    public User exitFromRoom(Integer id) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SET_NULL_CURRENT_ROOM);
            statement.setInt(1, id);
            int countRows = statement.executeUpdate();
            if (countRows != 1) {
                throw new SQLException("Не удалось выйти из комнаты");
            }
            return findById(id);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, null);
        }
    }
}
