package ru.itdrive.chat.Impl;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.chat.MessageService;
import ru.itdrive.db.model.Message;
import ru.itdrive.db.model.Room;
import ru.itdrive.db.model.User;
import ru.itdrive.db.repository.MessageRepository;
import ru.itdrive.db.repository.RoomRepository;
import ru.itdrive.db.repository.UserRepository;

import java.util.Optional;

@Component
@NoArgsConstructor
@AllArgsConstructor
public class MessageServiceImpl implements MessageService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private MessageRepository messageRepository;

    @Override
    public String exitRoom(Integer userId, Integer roomId) {
        String result = null;
        if (userId == null) {
            result = "Пользователь не определен";
        } else if (roomId == null || roomId == 0) {
            result = "Текущая комната не определена";
        } else {
            userRepository.exitFromRoom(userId);
            result = printRooms();
        }
        return result;
    }

    @Override
    public String chooseRoom(Integer userId, String nameRoom) {
        if (nameRoom != null) {
            if (userId == null) {
                return "Пользователь не определен";
            }
            User user = userRepository.findById(userId);
            if (user.getCurrentRoom() != null && user.getCurrentRoom().getId() != 0) {
                return "Сначала выйдите из текущей комнаты - -exit from room";
            } else {
                Room room = roomRepository.findByName(nameRoom);
                user.setCurrentRoom(room);
                user = userRepository.save(user);
                return printLastMessages(room.getId());
            }
        }
        return printRooms();
    }

    @Override
    public String sendMessage(Integer userId, Integer roomId, String message) {
        if (message != null) {
            if (roomId != null && roomId != 0) {
                return messageRepository.save(Message.builder()
                        .message(message)
                        .creator(User.builder().id(userId).build())
                        .room(Room.builder().id(roomId).build())
                        .build()).toString();
            } else {
                return printRooms();
            }
        }
        return null;
    }

    @Override
    public User start(String nickName) {
        User user = userRepository.findByNickName(nickName);
        if (user == null) {
            user = userRepository.save(User.builder()
                    .nickname(nickName)
                    .isActive(true)
                    .build());
        }
        return user;
    }

    @Override
    public String printRooms() {
        return ("Укажите комнату: -choose room <имя комнаты> \n" +
                roomRepository.findAll().stream().map(Room::toString).reduce("", (prev, next) -> (prev + "\n" + next)));
    }

    @Override
    public String printLastMessages(Integer roomId) {
        return (messageRepository.findLastMessages(roomId).stream()
                .map(Message::toString)
                .reduce("", (prev, next) -> (prev + "\n" + next)));
    }

    @Override
    public Integer getCurrentRoom(Integer userId) {
        return Optional.ofNullable(userRepository.findById(userId).getCurrentRoom()).map(Room::getId).orElse(null);
    }
}
