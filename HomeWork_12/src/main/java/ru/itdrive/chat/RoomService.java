package ru.itdrive.chat;

import ru.itdrive.db.model.Room;

public interface RoomService {
    void save(Room room);

    void delete(Integer id);
}
