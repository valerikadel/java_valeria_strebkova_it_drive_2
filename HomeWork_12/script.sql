create table chat_user
(
    id        serial primary key,
    nickname  char(20),
    is_active boolean default true
);
create table room
(
    id   serial primary key,
    name char(20)
);
create table message
(
    id            serial primary key,
    creation_date timestamp,
    message       char(500),
    creator_id    integer,
    room_id       integer,
    foreign key (creator_id) references chat_user (id),
    foreign key (room_id) references room (id)
);
alter table chat_user
    add column current_room_id integer;
alter table chat_user
    add constraint user_current_room
        FOREIGN KEY (current_room_id)
            REFERENCES room (id);

insert into room (name) values ('main');
insert into room (name) values ('spams');

alter table room add column if not exists max_capacity int;
