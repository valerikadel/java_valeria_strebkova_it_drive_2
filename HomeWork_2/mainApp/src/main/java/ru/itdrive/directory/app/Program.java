package ru.itdrive.directory.app;

import ru.itdrive.printer.directory.utils.ArgumentConverter;
import ru.itdrive.printer.directory.utils.CheckerFiles;

import java.io.IOException;

class Program {

    public static void main(String[] args) throws IOException {
        CheckerFiles checker = new CheckerFiles();
        checker.printFromDirectory(ArgumentConverter.getCorrectPath(args));
    }
}