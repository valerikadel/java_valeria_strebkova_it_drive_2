package ru.itdrive.directory.utils;

import com.beust.jcommander.JCommander;
import ru.itdrive.directory.app.FilePath;

public class ArgumentConverter {
    public static final String SPACE_DELIMITER = " ";

    public static String getCorrectPath(String[] args) {
        FilePath arguments = new FilePath();

        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        if (arguments.partOfPath != null) {
            String joinPartOfPath = String.join(SPACE_DELIMITER, arguments.partOfPath);
            return String.join(SPACE_DELIMITER, arguments.mainPartOfPath, joinPartOfPath);
        }
        return arguments.mainPartOfPath;
    }
}
