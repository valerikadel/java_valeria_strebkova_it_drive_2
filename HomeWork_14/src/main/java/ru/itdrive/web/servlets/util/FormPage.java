package ru.itdrive.web.servlets.util;

import lombok.AllArgsConstructor;
import lombok.Builder;

import java.util.List;

@AllArgsConstructor
@Builder
public class FormPage {
    private String method;
    private String action;
    private String enctype;
    private List<Input> inputs;

    public String buildForm() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("<form action=\"%s\" method=\"%s\" enctype=\"%s\">\n", action, method, enctype));
        inputs.forEach(input ->
        {
            sb.append(input.buildInput());
        });
        sb.append("</form>\n");
        return sb.toString();
    }

    @AllArgsConstructor
    @Builder
    public static class Input {
        private String type;
        private String name;
        private String value;

        private String buildInput() {
            return String.format(" <input type=\"%s\" " + (name != null ? "name=\"%s\">\n" : "value=\"%s\">\n"), type, name != null ? name : value);
        }
    }
}
