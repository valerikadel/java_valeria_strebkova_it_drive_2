package ru.itdrive.web.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.config.ApplicationConfig;
import ru.itdrive.model.User;
import ru.itdrive.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/signIn")
public class SignIn extends HttpServlet {
    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("html/signIn.html");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("login");
        String password = req.getParameter("password");
        User user = userService.findByLogin(name);
        if (user == null) {
            user = User.builder().password(password).login(name).build();
        }
        user = userService.save(user);
        Cookie cookie = new Cookie("userToken", user.getToken());
        resp.addCookie(cookie);
        resp.sendRedirect("/files");
    }

    @Override
    public void init() throws ServletException {
        ApplicationContext config = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        userService = config.getBean(UserService.class);
    }
}
