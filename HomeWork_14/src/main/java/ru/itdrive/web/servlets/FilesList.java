package ru.itdrive.web.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.config.ApplicationConfig;
import ru.itdrive.model.User;
import ru.itdrive.service.FileService;
import ru.itdrive.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/files")
@MultipartConfig
public class FilesList extends HttpServlet {

    private UserService userService;
    private FileService fileService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        User currentUser = getUserByToken(req.getCookies());
        if (currentUser == null) {
            resp.sendRedirect("/signIn");
        } else {
            writer.println(fileService.getPageWithFiles(currentUser));
        }
    }

    private User getUserByToken(Cookie[] cookies) {
        User currentUser = null;
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("userToken")) {
                currentUser = userService.findByToken(cookie.getValue());
                break;
            }
        }
        return currentUser;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User currentUser = getUserByToken(req.getCookies());
        Part part = req.getPart("file");
        fileService.save(currentUser, part.getSubmittedFileName(), part.getInputStream(), part.getContentType());
        resp.sendRedirect("/files");
    }

    @Override
    public void init() throws ServletException {
        ApplicationContext config = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        userService = config.getBean(UserService.class);
        fileService = config.getBean(FileService.class);
    }
}
