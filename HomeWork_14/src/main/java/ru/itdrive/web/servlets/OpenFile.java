package ru.itdrive.web.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.config.ApplicationConfig;
import ru.itdrive.model.File;
import ru.itdrive.service.FileService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;

@WebServlet("/openFile")
public class OpenFile extends HttpServlet {
    private FileService fileService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer id = Integer.valueOf(req.getParameter("id"));
        File fileDB = fileService.findById(id);
        java.io.File file = new java.io.File(fileDB.getLocalPath());
        Files.copy(file.toPath(), resp.getOutputStream());
        resp.setContentType(fileDB.getContentType());
        resp.setContentLength((int) file.length());
        resp.setHeader("Content-Disposition", "filename=\"" + file + "\"");
        resp.flushBuffer();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println();
    }

    @Override
    public void init() throws ServletException {
        ApplicationContext config = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        fileService = config.getBean(FileService.class);
    }
}
