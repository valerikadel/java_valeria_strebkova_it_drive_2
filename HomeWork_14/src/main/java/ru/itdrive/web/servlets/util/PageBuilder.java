package ru.itdrive.web.servlets.util;

import lombok.Builder;

import java.util.List;

@Builder
public class PageBuilder {
    private final String header = "<!doctype html>\n" +
            "<html lang=\"en\">\n" +
            "<head>\n" +
            "    <title>%s</title>\n" +
            "</head>\n" +
            "<body>\n";
    private final String endHeader = "</body>\n" +
            "</html>";
    private String urlTemplate;
    private Table table;
    private List<Object> additionalThings;

    public String buildPage() {
        StringBuilder createPage = new StringBuilder(header);
        StringBuilder additionalSB = new StringBuilder();
        if (table != null) {
            createPage.append(table.buildTable());
        }
        for (Object thing : additionalThings) {
            if (thing instanceof FormPage) {
                additionalSB.append(((FormPage) thing).buildForm());
            }
        }
        createPage.append(additionalSB.toString());
        createPage.append(endHeader);
        return createPage.toString();
    }
}
