package ru.itdrive.web.servlets.util;

import java.util.List;
import java.util.Map;

public class Table {
    private final String table = "<table border=\"1\">";
    private final String endTable = "</table>";
    private Map<Long, List<String>> rowCells;

    public void addRowCells(Map<Long, List<String>> information) {
        this.rowCells = information;
    }

    public String buildTable() {
        StringBuilder sb = new StringBuilder(table);
        rowCells.forEach((row, cells) -> {
            sb.append("<tr>");
            sb.append(String.format("<td>%s</td>", row));
            cells.forEach(c -> {
                sb.append(String.format("<td>%s</td>", c));
            });
            sb.append("</tr>");
        });
        sb.append(endTable);
        return sb.toString();
    }
}
