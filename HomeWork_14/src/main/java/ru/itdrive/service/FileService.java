package ru.itdrive.service;

import ru.itdrive.model.File;
import ru.itdrive.model.User;

import java.io.InputStream;
import java.util.List;

public interface FileService {
    List<File> getFiles(int userId);

    void delete(int fileId);

    File save(User currentUser, String fileName, InputStream inputStream, String contentType);

    File findById(Integer id);

    String getPageWithFiles(User currentUser);
}
