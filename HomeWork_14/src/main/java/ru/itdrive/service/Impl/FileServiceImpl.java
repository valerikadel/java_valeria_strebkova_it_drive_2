package ru.itdrive.service.Impl;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import ru.itdrive.model.File;
import ru.itdrive.model.User;
import ru.itdrive.repository.FileRepository;
import ru.itdrive.service.FileService;
import ru.itdrive.web.servlets.util.FormPage;
import ru.itdrive.web.servlets.util.PageBuilder;
import ru.itdrive.web.servlets.util.Table;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
@NoArgsConstructor
@AllArgsConstructor
@PropertySource("classpath:application.properties")
public class FileServiceImpl implements FileService {
    @Autowired
    private Environment environment;
    @Autowired
    private FileRepository fileRepository;

    @Override
    public List<File> getFiles(int userId) {
        return fileRepository.getFiles(userId);
    }

    @Override
    public void delete(int fileId) {
        fileRepository.delete(fileId);
    }

    @Override
    public File save(User currentUser, String fileName, InputStream inputStream, String contentType) {
        String path = Optional.ofNullable(environment.getProperty("file.path"))
                .orElseThrow(() -> new IllegalArgumentException("В свойствах не прописан путь к папке"));
        String fileNameWithPath = path + fileName;
        java.io.File parent = new java.io.File(path);
        if (parent.listFiles() != null) {
            for (java.io.File child : parent.listFiles()) {
                if (child.isFile() && child.getName().equals(fileName)) {
                    int last = fileName.lastIndexOf(".");
                    String name = fileName.substring(0, last);
                    String ext = fileName.substring(last);
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH.mm.ss");
                    fileNameWithPath = path + name + " " + LocalDateTime.now().format(formatter) + ext;
                }
            }
        }
        try {
            Files.copy(inputStream, Paths.get(fileNameWithPath));
        } catch (IOException e) {
            return null;
        }
        File file = File.builder()
                .localPath(fileNameWithPath)
                .fileName(fileName)
                .creator(currentUser)
                .contentType(contentType)
                .build();
        return fileRepository.save(file);
    }

    @Override
    public File findById(Integer id) {
        return fileRepository.findById(id);
    }

    @Override
    public String getPageWithFiles(User currentUser) {
        List<File> files = getFiles(currentUser.getId());
        String url = "<a href=\"%s\">%s</a>";
        Map<Long, List<String>> rowCells = new HashMap<>();
        Long i = 0L;
        for (File file : files) {
            String link = String.format(url, "http://localhost:8080/openFile?id=" + file.getId(), file.getFileName());
            rowCells.put(i, List.of(link));
            i++;
        }
        FormPage form = FormPage.builder()
                .action("/files")
                .method("post")
                .enctype("multipart/form-data")
                .inputs(List.of(FormPage.Input.builder()
                                .name("file")
                                .type("file")
                                .build(),
                        FormPage.Input.builder()
                                .type("submit").value("File Upload")
                                .build()))
                .build();
        Table table = new Table();
        table.addRowCells(rowCells);
        return PageBuilder.builder()
                .additionalThings(List.of(form))
                .table(table)
                .build().buildPage();
    }
}
