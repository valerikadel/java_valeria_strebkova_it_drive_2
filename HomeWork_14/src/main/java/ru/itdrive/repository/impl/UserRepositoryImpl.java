package ru.itdrive.repository.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import ru.itdrive.model.User;
import ru.itdrive.repository.DBClosing;
import ru.itdrive.repository.RowMapper;
import ru.itdrive.repository.UserRepository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;
import java.util.UUID;


@Component
@AllArgsConstructor
public class UserRepositoryImpl extends DBClosing implements UserRepository {
    private static final String SQL_INSERT = "insert into people (login, password, token) values (?, ?, uuid_generate_v4())";
    private static final String SQL_DELETE = "delete from people where id = ?";
    //language=SQL
    private static final String SQL_FIND_BY_LOGIN = "select u.id as uId, u.login as uLogin, u.password as uPassword, u.token as uToken " +
            "from people u " +
            "where lower(u.login) like ?";
    //language=SQL
    private static final String SQL_FIND_BY_TOKEN = "select u.id as uId, u.login as uLogin, u.password as uPassword, u.token as uToken " +
            "from people u " +
            "where u.token::varchar like ?";
    //language=SQL
    private static final String SQL_UPDATE = "update people set token = uuid_generate_v4() where id = ?";
    private final RowMapper<User> userRowMapper = row -> {
        return User.builder()
                .id(row.getInt("uId"))
                .login(row.getString("uLogin"))
                .password(row.getString("uPassword"))
                .token(row.getString("uToken"))
                .build();
    };
    private DataSource dataSource;

    @Override
    public User save(User object) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            connection = dataSource.getConnection();
            if (object.getId() == null) {
                statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, object.getLogin());
                statement.setString(2, object.getPassword());
                statement.executeUpdate();
                result = statement.getGeneratedKeys();
                if (result.next()) {
                    object.setId(result.getInt("id"));
                }
            } else {
                statement = connection.prepareStatement(SQL_UPDATE);
                statement.setInt(1, object.getId());
                statement.executeUpdate();
            }
            return findByLogin(object.getLogin());
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, result);
        }
    }

    @Override
    public void delete(Integer id) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_DELETE);
            statement.setInt(1, id);
            int countRows = statement.executeUpdate();
            if (countRows != 1) {
                throw new SQLException("Не удалось удалить пользователя");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, null);
        }
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public User findByLogin(String name) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_BY_LOGIN);
            statement.setString(1, name.trim().toLowerCase());
            result = statement.executeQuery();
            if (result.next()) {
                return userRowMapper.mapRow(result);
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, result);
        }
    }

    @Override
    public User findByToken(String token) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_BY_TOKEN);
            statement.setString(1, token);
            result = statement.executeQuery();
            if (result.next()) {
                return userRowMapper.mapRow(result);
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, result);
        }
    }
}
