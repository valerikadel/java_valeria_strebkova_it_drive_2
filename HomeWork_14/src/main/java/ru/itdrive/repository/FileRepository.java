package ru.itdrive.repository;

import ru.itdrive.model.File;

import java.util.List;

public interface FileRepository extends CrudRepository<File> {
    List<File> getFiles(int userId);

    File findById(Integer id);
}
