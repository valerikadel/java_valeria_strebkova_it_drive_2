package ru.itdrive.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class File {
    private Integer id;
    private String fileName;
    private User creator;
    private String localPath;
    private String contentType;
}
