package ru.itdrive.model;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class User {
    private Integer id;
    private String login;
    private String password;
    private String token;
    private Set<File> files;
}
