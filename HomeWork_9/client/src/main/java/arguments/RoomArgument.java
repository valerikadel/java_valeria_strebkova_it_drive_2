package arguments;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class RoomArgument {
    @Parameter(names = "-choose room")
    public String roomName;
}
