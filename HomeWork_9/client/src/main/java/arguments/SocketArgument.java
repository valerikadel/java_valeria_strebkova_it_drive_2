package arguments;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class SocketArgument {
    @Parameter(names = "--host")
    public String host;
    @Parameter(names = "--port")
    public String port;
}
