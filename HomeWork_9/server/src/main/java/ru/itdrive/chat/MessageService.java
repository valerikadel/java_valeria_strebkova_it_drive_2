package ru.itdrive.chat;

import ru.itdrive.db.model.User;

import java.sql.SQLException;

public interface MessageService {
    String exitRoom(Integer userId, Integer roomId) throws SQLException, ClassNotFoundException;

    String chooseRoom(Integer userId, String nameRoom) throws SQLException, ClassNotFoundException;

    String sendMessage(Integer userId, Integer roomId, String message) throws SQLException, ClassNotFoundException;

    User start(String nickName) throws SQLException, ClassNotFoundException;

    String printLastMessages(Integer roomId);

    String printRooms();

    Integer getCurrentRoom(Integer userId);
}
