package ru.itdrive.chat;

public enum TypeCommand {
    EXIT_ROOM,
    CHOOSE_ROOM,
    CREATE_USER,
    MESSAGE;
}
