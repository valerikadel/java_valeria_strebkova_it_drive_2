package ru.itdrive.server;

import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;
import ru.itdrive.chat.MessageService;
import ru.itdrive.chat.TypeCommand;
import ru.itdrive.db.model.Room;
import ru.itdrive.db.model.User;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import static java.util.Optional.ofNullable;
import static ru.itdrive.chat.TypeCommand.*;

@NoArgsConstructor
public class ServerSocket {
    private final String COMMAND_CHOOSE_ROOM = "choose room";
    private final String COMMAND_EXIT_ROOM = "exit room";
    List<ChatSocketClient> antonyms;
    Map<Integer, List<ChatSocketClient>> users;
    MessageService messageService;

    public ServerSocket(MessageService messageService) {
        this.antonyms = new CopyOnWriteArrayList<>();
        this.users = new ConcurrentHashMap<>();
        this.messageService = messageService;
    }

    public void start() throws IOException {
        java.net.ServerSocket serverSocket = new java.net.ServerSocket(7777);
        while (true) {
            Socket socket = serverSocket.accept();
            ChatSocketClient client = new ChatSocketClient(socket);
            antonyms.add(client);
            client.start();
        }
    }

    private class ChatSocketClient extends Thread {
        private Integer userId;
        private Integer roomId;
        private java.net.Socket socket;
        private BufferedReader input;
        private PrintWriter output;
        private TypeCommand typeCommand;

        public ChatSocketClient(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try {
                String encoding = System.getProperty("console.encoding", "utf-8"); //пробы кодировки
                InputStream clientInputStream = socket.getInputStream();
                input = new BufferedReader(new InputStreamReader(clientInputStream, encoding));
                output = new PrintWriter(socket.getOutputStream(), true, Charset.forName(encoding));
                while (true) {
                    String inputLine = input.readLine();
                    if (inputLine != null) {
                        String commandLine = parseCommand(inputLine);
                        if (userId == null) {
                            findOrCreateUser(inputLine);
                        } else {
                            doCommand(commandLine);
                        }
                    }
                }
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        }

        private void findOrCreateUser(String inputLine) throws SQLException, ClassNotFoundException {
            User user = messageService.start(inputLine);
            roomId = Optional.ofNullable(user.getCurrentRoom()).map(Room::getId).orElse(null);
            userId = user.getId();
            if (roomId != null && roomId != 0) {
                updateUsersAndAntonyms();
                output.println(messageService.printLastMessages(roomId));
            } else {
                output.println(messageService.printRooms());
            }
        }

        private void doCommand(String commandLine) throws SQLException, ClassNotFoundException {
            switch (typeCommand) {
                case MESSAGE:
                    ofNullable(messageService.sendMessage(userId, roomId, commandLine)).ifPresent(this::sendAll);
                    break;
                case EXIT_ROOM:
                    output.println(messageService.exitRoom(userId, roomId));
                    removeFromUsers();
                    roomId = null;
                    break;
                case CHOOSE_ROOM:
                    output.println(messageService.chooseRoom(userId, commandLine));
                    roomId = messageService.getCurrentRoom(userId);
                    updateUsersAndAntonyms();
                    break;
                default:
                    this.output.println("Неизвестная команда");
            }
        }

        private void updateUsersAndAntonyms() {
            List<ChatSocketClient> usersInRoom = users.get(roomId);
            if (!CollectionUtils.isEmpty(usersInRoom)) {
                usersInRoom.add(this);
            } else {
                usersInRoom = new ArrayList<>() {
                };
                usersInRoom.add(this);
                users.put(roomId, usersInRoom);
                antonyms.remove(this);
            }
        }

        private void removeFromUsers() {
            List<ChatSocketClient> usersInRoom = users.get(roomId);
            if (!CollectionUtils.isEmpty(usersInRoom)) {
                usersInRoom.remove(this);
                antonyms.add(this);
            }
        }

        private String parseCommand(String inputLine) {
            String commandLine = null;
            typeCommand = inputLine.toLowerCase().equals(COMMAND_EXIT_ROOM) ? EXIT_ROOM : null;
            if (typeCommand == null && inputLine.length() > COMMAND_CHOOSE_ROOM.length()
                    && inputLine.substring(0, COMMAND_CHOOSE_ROOM.length()).trim().toLowerCase().equals(COMMAND_CHOOSE_ROOM)) {
                typeCommand = CHOOSE_ROOM;
                commandLine = inputLine.substring(COMMAND_CHOOSE_ROOM.length());
            }
            if (typeCommand == null || inputLine.equals(commandLine)) {
                typeCommand = MESSAGE;
                commandLine = inputLine;
            }
            return commandLine;
        }

        private void sendAll(String message) {
            users.forEach((currentRoomId, clients) -> {
                if (currentRoomId.equals(roomId)) {
                    clients.forEach(client -> client.output.println(message));
                }
            });
        }
    }
}
