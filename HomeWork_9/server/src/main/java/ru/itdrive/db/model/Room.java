package ru.itdrive.db.model;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class Room {
    private Integer id;
    private String name;
    private Set<User> users;
    private Set<Message> messages;

    @Override
    public String toString() {
        return
                "id=" + id +
                        ", name='" + name;
    }
}
