package ru.itdrive.db.model;

import lombok.Builder;
import lombok.Data;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

@Data
@Builder
public class Message {
    private Integer id;
    private Timestamp creationDate;
    private User creator;
    private Room room;
    private String message;

    @Override
    public String toString() {
        return "from:" + creator.getNickname().trim() +
                ", creationDate=" + new SimpleDateFormat().format(creationDate) +
                "\n" + message.trim();
    }
}
