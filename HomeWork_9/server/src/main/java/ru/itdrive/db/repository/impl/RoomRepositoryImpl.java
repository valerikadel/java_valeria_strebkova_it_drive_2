package ru.itdrive.db.repository.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import ru.itdrive.db.model.Room;
import ru.itdrive.db.repository.DBClosing;
import ru.itdrive.db.repository.RoomRepository;
import ru.itdrive.db.repository.RowMapper;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


@Component
@AllArgsConstructor
public class RoomRepositoryImpl extends DBClosing implements RoomRepository {

    private static final String SQL_INSERT = "insert into room (name) values (?)";
    private static final String SQL_DELETE = "delete from room where id=?";
    private static final String SQL_FIND_ALL = "select r.id as rId, r.name as rName from room r";
    private static final String SQL_FIND_BY_NAME = "select r.id as rId, r.name as rName from room r where lower(r.name) like ? ";
    private final RowMapper<Room> roomRowMapper = row -> Room.builder()
            .id(row.getInt("rId"))
            .name(row.getString("rName"))
            .build();
    private DataSource dataSource;

    @Override
    public Room save(Room object) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            connection = dataSource.getConnection();
            if (object.getId() == null) {
                statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, object.getName());
                statement.executeUpdate();
                result = statement.getGeneratedKeys();
                if (result.next()) {
                    object.setId(result.getInt("id"));
                }
            }
            return object;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, result);
        }
    }

    @Override
    public void delete(Integer id) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_DELETE);
            statement.setInt(1, id);
            int countRows = statement.executeUpdate();
            if (countRows != 1) {
                throw new SQLException("Не удалось удалить комнату");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, null);
        }
    }

    @Override
    public List<Room> findAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_ALL);
            result = statement.executeQuery();
            List<Room> rooms = new ArrayList<>();
            while (result.next()) {
                rooms.add(roomRowMapper.mapRow(result));
            }
            return rooms;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, result);
        }
    }

    @Override
    public Room findByName(String name) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_BY_NAME);
            statement.setString(1, "%" + name.trim().toLowerCase() + "%");
            result = statement.executeQuery();
            Room room = null;
            if (result.next()) {
                room = roomRowMapper.mapRow(result);
            }
            return room;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(connection, statement, result);
        }
    }
}
