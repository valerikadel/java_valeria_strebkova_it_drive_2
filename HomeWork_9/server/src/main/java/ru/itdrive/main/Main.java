package ru.itdrive.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.chat.MessageService;
import ru.itdrive.config.ApplicationConfig;
import ru.itdrive.server.ServerSocket;

import java.io.IOException;

public class Main {
    private static final int PORT = 7777;
    private static final String HOST = "localhost";

    public static void main(String[] args) {
        try {
            ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
            MessageService messageService = context.getBean(MessageService.class);
            ServerSocket serverSocket = new ServerSocket(messageService);
            serverSocket.start();
        } catch (NumberFormatException | IOException e) {
            System.out.println("Указанный порт не является числом");
        }
    }
}
