package ru.itdrive.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class Course {
    private Integer id;
    private String title;
    private Timestamp start_date;
    private Timestamp finish_date;

    private List<Lesson> lessons;

    public Course(Integer id, String title, Timestamp start_date, Timestamp finish_date) {
        this.id = id;
        this.title = title;
        this.start_date = start_date;
        this.finish_date = finish_date;
        this.lessons = new ArrayList<>();
    }

    public List<Lesson> getLessons() {
        return this.lessons;
    }

    public Integer getId() {
        return this.id;
    }

    @Override
    public String toString() {
        return "\nCourse{" +
                "id=" + id +
                ",\n title='" + title.trim() + '\'' +
                ",\n start_date=" + start_date +
                ",\n finish_date=" + finish_date +
                ",\n\t lessons=" + lessons +
                '}';
    }

    public String toStringWithoutLink() {
        return "\nCourse{" +
                "id=" + id +
                ",\n title='" + title.trim() + '\'' +
                ",\n start_date=" + start_date +
                ",\n finish_date=" + finish_date +
                '}';
    }

}
