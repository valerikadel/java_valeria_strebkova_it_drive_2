package ru.itdrive.repository;

import ru.itdrive.model.Course;

public interface CourseRepository extends CrudRepository<Course> {
}
