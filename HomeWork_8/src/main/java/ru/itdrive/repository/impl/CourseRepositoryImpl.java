package ru.itdrive.repository.impl;

import ru.itdrive.model.Course;
import ru.itdrive.model.Lesson;
import ru.itdrive.repository.CourseRepository;
import ru.itdrive.repository.RowMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CourseRepositoryImpl implements CourseRepository {
    //language=SQL
    private static String SQL_FIND_BY_ID = "select c.id as cId, c.title as cTitle,c.start_date as startDate," +
            "c.finish_date as finishDate," +
            "l.id as lId,l.name as lName from course c " +
            "left join lesson l on c.id=l.course_id where c.id = ";
    //language=SQL
    private static String SQL_FIND_ALL = "select c.id as cId, c.title as cTitle,c.start_date as startDate," +
            "c.finish_date as finishDate," +
            "l.id as lId,l.name as lName from course c " +
            "left join lesson l on c.id=l.course_id";

    private Connection connection;
    private RowMapper<Course> courseRowMapper = new RowMapper<Course>() {
        @Override
        public Course mapRow(ResultSet row) throws SQLException {
            return new Course(row.getInt("cId"),
                    row.getString("cTitle"),
                    row.getTimestamp("startDate"),
                    row.getTimestamp("finishDate"));
        }
    };
    private RowMapper<Lesson> lessonRowMapper = new RowMapper<Lesson>() {
        @Override
        public Lesson mapRow(ResultSet row) throws SQLException {
            return new Lesson(row.getInt("lId"),
                    row.getString("lName")
            );
        }
    };

    public CourseRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Course find(Integer id) {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID + id);
            ResultSet resultSet = statement.executeQuery();
            Course course = null;
            while (resultSet.next()) {
                if (course == null) {
                    course = courseRowMapper.mapRow(resultSet);
                }
                course.getLessons().add(lessonRowMapper.mapRow(resultSet));
            }
            statement.close();
            resultSet.close();
            return course;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Course> findAll() {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL);
            ResultSet resultSet = statement.executeQuery();
            List<Course> result = new ArrayList<>();
            Course course = null;
            while (resultSet.next()) {
                if (course != null && (resultSet.getInt("cId") != course.getId())) {
                    result.add(course);
                    course = null;
                }
                if (course == null) {
                    course = courseRowMapper.mapRow(resultSet);
                }
                course.getLessons().add(lessonRowMapper.mapRow(resultSet));
            }
            result.add(course);
            statement.close();
            resultSet.close();
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
