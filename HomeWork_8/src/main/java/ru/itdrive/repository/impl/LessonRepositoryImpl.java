package ru.itdrive.repository.impl;

import ru.itdrive.model.Course;
import ru.itdrive.model.Lesson;
import ru.itdrive.repository.LessonRepository;
import ru.itdrive.repository.RowMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LessonRepositoryImpl implements LessonRepository {
    //language=SQL
    private static String SQL_FIND_BY_ID = "select l.id as lId,l.name as lName,c.id as cId,c.title as cTitle," +
            "c.start_date as startDate,c.finish_date as finishDate from lesson l " +
            "left join course c on c.id=l.course_id where l.id = ";
    //language=SQL
    private static String SQL_FIND_ALL = "select l.id as lId,l.name as lName,c.id as cId,c.title as cTitle," +
            " c.start_date as startDate,c.finish_date as finishDate from lesson l " +
            "inner join course c on c.id=l.course_id";

    private Connection connection;
    private RowMapper<Lesson> lessonRowMapper = new RowMapper<Lesson>() {
        @Override
        public Lesson mapRow(ResultSet row) throws SQLException {
            return new Lesson(row.getInt("lId"),
                    row.getString("lName"),
                    new Course(row.getInt("cId"),
                            row.getString("cTitle"),
                            row.getTimestamp("startDate"),
                            row.getTimestamp("finishDate")
                    )
            );
        }
    };

    public LessonRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Lesson find(Integer id) {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID + id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Lesson result = lessonRowMapper.mapRow(resultSet);
            statement.close();
            resultSet.close();
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Lesson> findAll() {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL);
            ResultSet resultSet = statement.executeQuery();
            List<Lesson> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(lessonRowMapper.mapRow(resultSet));
            }
            statement.close();
            resultSet.close();
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
