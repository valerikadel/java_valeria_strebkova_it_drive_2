package ru.itdrive;

import ru.itdrive.repository.CourseRepository;
import ru.itdrive.repository.LessonRepository;
import ru.itdrive.repository.impl.CourseRepositoryImpl;
import ru.itdrive.repository.impl.LessonRepositoryImpl;

import java.sql.Connection;
import java.sql.DriverManager;

public class Main {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/courses";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "1234";

    public static void main(String[] args) throws Exception {
        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

        LessonRepository lessonRepository = new LessonRepositoryImpl(connection);
        CourseRepository courseRepository = new CourseRepositoryImpl(connection);

        System.out.println("Results:\n");
        System.out.println("Lesson findById:\n");
        System.out.println(lessonRepository.find(1).toStringWithLink());
        System.out.println("Lesson findAll:\n");
        lessonRepository.findAll().forEach(l -> System.out.println(l.toStringWithLink()));
        System.out.println("Course findById:\n");
        System.out.println(courseRepository.find(2));
        System.out.println("Course findAll:\n");
        System.out.println(courseRepository.findAll());
    }

}
