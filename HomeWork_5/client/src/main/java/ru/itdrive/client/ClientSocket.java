package ru.itdrive.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientSocket {
    private Socket client;
    private PrintWriter writerToServer;
    private BufferedReader readerFromServer;

    public ClientSocket(String host, int port) {
        try {
            client = new Socket(host, port);
            writerToServer = new PrintWriter(client.getOutputStream(), true);
            readerFromServer = new BufferedReader(new InputStreamReader(client.getInputStream()));
            new Thread(receiverMessages).start();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void sendToServer(String message) {
        writerToServer.println(message);
    }

    private Runnable receiverMessages = new Runnable() {
        public void run() {
            while (true) {
                try {
                    String messageFromServer = readerFromServer.readLine();
                    if (messageFromServer != null) {
                        System.out.println(messageFromServer);
                    }
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    };
}
