package ru.itdrive.client;

import arguments.SocketArgument;
import com.beust.jcommander.JCommander;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        SocketArgument argument = new SocketArgument();
        JCommander.newBuilder()
                .addObject(argument)
                .build()
                .parse(args);
        try {
            int port = Integer.parseInt(argument.port);
            Scanner scanner = new Scanner(System.in);

            ClientSocket clientSocket = new ClientSocket(argument.host, port);
            while (true) {
                String message = scanner.nextLine();
                clientSocket.sendToServer(message);
            }
        } catch (NumberFormatException e) {
            System.out.println("Указанный порт не является числом");
        }
    }
}
