package ru.itdrive.server;

import java.io.*;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ServerSocket {
    private List<ClientRunnable> clients;

    public ServerSocket() {
        this.clients = new CopyOnWriteArrayList<>();
    }

    public void start(int port) {
        java.net.ServerSocket server;
        try {
            server = new java.net.ServerSocket(port);
            while (true) {
                ClientRunnable run = new ClientRunnable(server.accept(), server);
                clients.add(run);
                run.start();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


    public class ClientRunnable extends Thread {
        private java.net.ServerSocket serverSocket;
        private Socket client;
        private PrintWriter printWriter;

        public ClientRunnable(Socket client, java.net.ServerSocket serverSocket) {
            this.client = client;
            this.serverSocket = serverSocket;
        }

        protected PrintWriter getPrintWriter() {
            return this.printWriter;
        }

        public void run() {
            try {
                InputStream clientInputStream = client.getInputStream();
                BufferedReader clientReader = new BufferedReader(new InputStreamReader(clientInputStream));
                printWriter = new PrintWriter(client.getOutputStream(), true);
                while (true) {
                    String inputLine = clientReader.readLine();
                    if (inputLine != null) {
                        clients.stream().map(ClientRunnable::getPrintWriter).forEach(writer -> {
                            if (writer != null) {
                                writer.println(inputLine);
                            }
                        });
                    }
                }
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        }
    }
}
