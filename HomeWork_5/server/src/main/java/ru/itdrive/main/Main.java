package ru.itdrive.main;

import com.beust.jcommander.JCommander;
import ru.itdrive.argument.ServerArgument;
import ru.itdrive.server.ServerSocket;

public class Main {
    public static void main(String[] args) {
        ServerArgument argument = new ServerArgument();
        JCommander.newBuilder()
                .addObject(argument)
                .build()
                .parse(args);
        try {
            int port = Integer.parseInt(argument.port);
            ServerSocket serverSocket = new ServerSocket();
            serverSocket.start(port);
        } catch (NumberFormatException e) {
            System.out.println("Указанный порт не является числом");
        }
    }
}
