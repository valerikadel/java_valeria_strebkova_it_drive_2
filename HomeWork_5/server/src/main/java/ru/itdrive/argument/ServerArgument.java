package ru.itdrive.argument;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class ServerArgument {
    @Parameter(names = "--port")
    public String port;
}
