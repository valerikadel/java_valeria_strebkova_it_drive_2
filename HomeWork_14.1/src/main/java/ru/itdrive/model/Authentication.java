package ru.itdrive.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Authentication {
    private String email;
    private String password;
    private Role role;
}
