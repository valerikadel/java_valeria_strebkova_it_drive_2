package ru.itdrive.service;

import ru.itdrive.model.User;

import java.util.List;

public interface UserService {
    User save(User user);

    void delete(Integer id);

    User findByLogin(String login);

    User findByToken(String token);

    List<User> getAll();
}
