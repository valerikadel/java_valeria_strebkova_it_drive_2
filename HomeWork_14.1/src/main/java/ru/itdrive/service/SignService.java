package ru.itdrive.service;

import ru.itdrive.model.Authentication;

public interface SignService {
    Authentication authenticate(String login, String password);

    Authentication register(String login, String password);
}
