package ru.itdrive.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.model.Authentication;
import ru.itdrive.model.User;
import ru.itdrive.repository.UserRepository;
import ru.itdrive.service.SignService;

import java.util.Optional;

@Component
public class SignServiceImpl implements SignService {
    @Autowired
    UserRepository userRepository;

    @Override
    public Authentication authenticate(String login, String password) {
        User user = userRepository.findByLogin(login);
        if (Optional.ofNullable(user).isPresent()) {
            return Authentication.builder()
                    .email(user.getLogin())
                    .password(user.getPassword())
                    .role(user.getRole()).build();
        } else {
            return register(login, password);
        }
    }

    @Override
    public Authentication register(String login, String password) {
        User newUser = userRepository.save(User.builder().login(login).password(password).build());
        return Authentication.builder().password(newUser.getPassword()).email(newUser.getLogin()).role(newUser.getRole()).build();
    }
}
