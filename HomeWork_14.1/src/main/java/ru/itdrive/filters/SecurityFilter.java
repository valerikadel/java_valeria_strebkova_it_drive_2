package ru.itdrive.filters;

import ru.itdrive.model.Authentication;
import ru.itdrive.model.Role;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

@WebFilter("/*")
public class SecurityFilter implements Filter {
    private final List<String> alwaysPermit = Arrays.asList("/signin", "/signup", "/logout");

    private final Map<String, List<String>> permissions = new HashMap<>();

    private Properties properties;

    @Override
    public void init(FilterConfig filterConfig) {
        properties = (Properties) filterConfig.getServletContext().getAttribute("properties");
        properties.forEach((k, v) -> {
            String key = (String) k;
            String value = (String) v;
            permissions.put(key.substring("permissions".length() + 1), Arrays.asList(Optional.ofNullable(value)
                    .map(a -> a.toLowerCase().split(","))
                    .orElse(new String[0])));
        });
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);
        if (alwaysPermit.contains(request.getRequestURI().toLowerCase())) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        if (session != null) {
            Authentication authentication = (Authentication) session.getAttribute("authentication");
            if (authentication != null) {
                Role role = authentication.getRole();
                if (isPermitted(request.getRequestURI(), role)) {
                    filterChain.doFilter(servletRequest, servletResponse);
                    return;
                } else {
                    //sendforb
                    response.setStatus(403);
                    response.sendRedirect("/signIn");
                    return;
                }
            }
            //send to sign
            response.sendRedirect("/signIn");
            return;
        } else {
            //send to sign
            response.setStatus(403);
            response.sendRedirect("/signIn");
        }
        return;
    }

    private boolean isPermitted(String path, Role role) {
        List<String> permissionByRole = permissions.computeIfAbsent(role.name().toLowerCase(), k -> new ArrayList<>());
        if (permissionByRole.isEmpty() || permissionByRole.contains(path.toLowerCase()) || alwaysPermit.contains(path.toLowerCase())) {
            return true;
        }
        return false;
    }

    @Override
    public void destroy() {

    }
}
