package ru.itdrive.repository;

import ru.itdrive.model.User;

import java.util.List;

public interface UserRepository extends CrudRepository<User> {
    User findByLogin(String login);

    User findByToken(String token);

    List<User> getAll();
}
