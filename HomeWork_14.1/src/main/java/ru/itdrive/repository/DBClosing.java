package ru.itdrive.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static java.util.Optional.ofNullable;

public abstract class DBClosing {
    protected void close(Connection connection, PreparedStatement statement, ResultSet result) {
        ofNullable(result).ifPresent(r -> {
            try {
                r.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
        ofNullable(statement).ifPresent(st -> {
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
        ofNullable(connection).ifPresent(con -> {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }
}
