package ru.itdrive.web.servlets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.itdrive.model.Authentication;
import ru.itdrive.service.SignService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

//@WebServlet("/signUp")
public class SignUp extends HttpServlet {

    @Autowired
    private SignService signService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("jsp/signIn.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        Optional<Authentication> user = Optional.ofNullable((Authentication) session.getAttribute("authentication"));
        if (user.isPresent()) {
            RedirectUtil.sendRedirect(req, resp, user.get());
        } else {
            String login = req.getParameter("login");
            String password = req.getParameter("password");
            Authentication authentication = signService.register(login, password);
            session = req.getSession();
            session.setAttribute("authentication", authentication);
            if (authentication != null) {
                RedirectUtil.sendRedirect(req, resp, authentication);
            }
        }
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springContext");
        signService = applicationContext.getBean(SignService.class);
    }
}
