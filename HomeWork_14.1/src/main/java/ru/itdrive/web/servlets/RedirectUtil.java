package ru.itdrive.web.servlets;

import ru.itdrive.model.Authentication;
import ru.itdrive.model.Role;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

public class RedirectUtil {
    public static void sendRedirect(HttpServletRequest req, HttpServletResponse resp, Authentication authentication) {

        if (Optional.ofNullable(req.getParameter("redirect")).isPresent()) {
            try {
                resp.sendRedirect(req.getParameter("redirect"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                if (authentication == null) {
                    resp.sendRedirect("/signIn");
                } else if (authentication.getRole().equals(Role.USER)) {
                    resp.sendRedirect("/unusual");
                } else {
                    resp.sendRedirect("/users");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
