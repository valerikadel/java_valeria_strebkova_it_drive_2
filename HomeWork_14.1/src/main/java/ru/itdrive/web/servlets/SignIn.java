package ru.itdrive.web.servlets;

import org.springframework.context.ApplicationContext;
import ru.itdrive.model.Authentication;
import ru.itdrive.service.SignService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

import static ru.itdrive.web.servlets.RedirectUtil.sendRedirect;

@WebServlet("/signIn")
public class SignIn extends HttpServlet {
    private SignService signInService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("jsp/signIn.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        Optional<Authentication> user = Optional.ofNullable(session).map(s -> (Authentication) s.getAttribute("authentication"));
        String login = req.getParameter("login");
        if (user.isPresent() && user.get().getEmail().equals(login)) {
            sendRedirect(req, resp, user.get());
        } else {
            String password = req.getParameter("password");
            Authentication authentication = signInService.authenticate(login, password);
            session = req.getSession();
            session.setAttribute("authentication", authentication);
            sendRedirect(req, resp, authentication);
        }
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");
        signInService = springContext.getBean(SignService.class);
    }
}
