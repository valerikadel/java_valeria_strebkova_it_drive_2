<%--
  Created by IntelliJ IDEA.
  User: StrebkovaVV
  Date: 16.11.2020
  Time: 19:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Users</title>
</head>
<body>

<table border="1">
    <tr>
        <td> Login</td>
        <td> Password</td>
        <td> Role</td>
    </tr>
    <jsp:useBean id="users" scope="request" type="java.util.List"/>
    <c:forEach var="user" items="${users}">
        <tr>
            <td>
                    ${user.login}
            </td>
            <td>
                    ${user.password}
            </td>
            <td>
                    ${user.role}
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
