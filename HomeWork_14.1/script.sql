create schema public;

comment on schema public is 'standard public schema';

alter schema public owner to postgres;

create table if not exists people
(
	id serial not null
		constraint people_pk
			primary key,
	login varchar(25) not null,
	password bit varying(30) not null,
	token uuid
);

alter table people owner to postgres;

create unique index if not exists people_id_uindex
	on people (id);

create table if not exists user_file
(
	id serial not null
		constraint user_file_pk
			primary key,
	user_id integer
		constraint user_file_people_id_fk
			references people,
	file_name varchar(125) not null,
	local_path varchar(500),
	content_type varchar (100)
);

alter table user_file owner to postgres;

create unique index if not exists user_file_id_uindex
	on user_file (id);

	CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

alter table people add column role varchar(30) default 'USER';
