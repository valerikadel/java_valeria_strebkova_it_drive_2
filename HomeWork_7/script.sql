create table student
(
    id         serial primary key,
    first_name char(20),
    last_name  char(20),
    age        integer,
    is_active  bool
);

insert into student (first_name, last_name, age, is_active)
values ('Марсель', 'Сидиков', 25, TRUE);

insert into student (first_name, last_name, age, is_active)
values ('Студент 1', 'Студент 1', 18, true),
       ('Студент 2', 'Студент 2', 19, false);

create table course
(
    id          serial primary key,
    title       char(20),
    start_date  timestamp,
    finish_date timestamp
);

insert into course(title, start_date, finish_date)
values ('Math', '2020-02-22', '2020-04-22');

insert into course(title, start_date, finish_date)
values ('Languages', '2020-01-10', '2020-02-20');

create table lesson
(
    id        serial primary key,
    name      char(20),
    course_id integer,
    foreign key (course_id) references course (id)
);

insert into lesson (name, course_id)
values ('Simple Math', 1);

insert into lesson (name, course_id)
values ('Русский язык', 2),
       ('English', 2),
       ('Deutsch', 2);

create table student_course
(
    student_id integer,
    course_id  integer,
    foreign key (student_id) references student (id),
    foreign key (course_id) references course (id)
);

insert into student_course (student_id, course_id)
values (1, 1),
       (1, 2),
       (2, 1);
