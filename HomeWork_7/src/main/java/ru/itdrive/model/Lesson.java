package ru.itdrive.model;

public class Lesson {
    private Integer id;
    private String name;
    private Course course;

    public Lesson(Integer id, String name, Course course) {
        this.id = id;
        this.name = name;
        this.course = course;
    }

    public Lesson(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "\tLesson{id=" + id +
                " name='" + name.trim() + '\'' +
                "}\n";
    }

    public String toStringWithLink() {

        return "\tLesson{" +
                "id=" + id +
                ",name='" + name.trim() + '\'' +
                ", course=" + course.toStringWithoutLink() +
                "}\n";
    }
}
