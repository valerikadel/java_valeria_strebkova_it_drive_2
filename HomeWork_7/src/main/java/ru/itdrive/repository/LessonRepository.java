package ru.itdrive.repository;

import ru.itdrive.model.Lesson;

public interface LessonRepository extends CrudRepository<Lesson> {
}
