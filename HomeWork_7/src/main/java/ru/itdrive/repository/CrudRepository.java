package ru.itdrive.repository;

import java.util.List;

public interface CrudRepository<T> {
    // T save(T object);

    // void update(T object);

    // void delete(Integer id);

    T find(Integer id);

    List<T> findAll();
}
