create database great_chat; --done in psql
create table talker (
    id serial primary key,
    first_name char(20),
    last_name char(20),
    username char(20),
    age integer,
    is_active bool default true,
    email char(50) not null,
    password char(20) not null
);

create table chat (
    id serial primary key,
    name char(20),
    creation_date date default now(),
    archived_date timestamp default null,
    creator_id integer, --who created
    foreign key (creator_id) references talker(id)
);

create table char_talker (
    id serial primary key,
    chat_id integer not null, --chat
    talker_id integer not null, --user
    foreign key (chat_id) references chat(id),
    foreign key (talker_id) references talker(id)
);

create table message (
    id serial primary key,
    chat_talker_id integer not null, --author-creator message
    text char(500), --text message
    creation_date timestamp default now(),
    sending_date timestamp, --when was send
    is_deleted bool default false,
    state char(10), --state of sending
    foreign key (chat_talker_id) references char_talker(id)
);